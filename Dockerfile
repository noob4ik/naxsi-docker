ARG NGINX_VERSION
FROM buildpack-deps:bionic AS builder
ARG NAXSI_VERSION
ARG NGINX_VERSION

WORKDIR  /usr/local/src/
RUN wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
 && tar xvfz nginx-${NGINX_VERSION}.tar.gz \
 && git clone https://github.com/nbs-system/naxsi.git \
 && cd nginx-${NGINX_VERSION} \
 && ./configure --with-compat --add-dynamic-module=../naxsi/naxsi_src \
 && make modules

FROM nginx:${NGINX_VERSION}
ARG NGINX_VERSION

COPY --from=builder /usr/local/src/nginx-${NGINX_VERSION}/objs/ngx_http_naxsi_module.so /etc/nginx/modules

COPY nginx/nginx.conf /etc/nginx/nginx.conf

ENV TZ=Europe/Moscow
